# cuts out the main contours of our source images then crops and rotates
import glob
import os.path
import sqlite3

import cv2
import numpy as np

BLUR = 21
CANNY_THRESH_1 = 10
CANNY_THRESH_2 = 200
MASK_DILATE_ITER = 10
MASK_ERODE_ITER = 10
MASK_COLOR = (0.0,0.0,1.0) # In BGR format

src_imgs = glob.glob("img-src/*")

db = sqlite3.connect("ratios.db")
dbc = db.cursor()

for src in src_imgs:
    img_color = cv2.imread(src)
    img_gray = cv2.imread(src, flags=cv2.IMREAD_GRAYSCALE)

    # add some white around the edges so we have some space to rotate
    img_color = cv2.copyMakeBorder(img_color,10,10,10,10,cv2.BORDER_CONSTANT, value=[255, 255, 255])
    img_gray = cv2.copyMakeBorder(img_gray,10,10,10,10,cv2.BORDER_CONSTANT, value=[255])
    
    
    # most of this is ganked directly from this https://stackoverflow.com/questions/29313667/how-do-i-remove-the-background-from-this-kind-of-image
    
    #-- Edge detection -------------------------------------------------------------------
    edges = cv2.Canny(img_gray, CANNY_THRESH_1, CANNY_THRESH_2)
    edges = cv2.dilate(edges, None)
    edges = cv2.erode(edges, None)
    
    #-- Find contours in edges, sort by area ---------------------------------------------
    contour_info = []
    contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for c in contours:
        contour_info.append((
            c,
            cv2.isContourConvex(c),
            cv2.contourArea(c),
        ))
    contour_info = sorted(contour_info, key=lambda c: c[2], reverse=True)
    max_contour = contour_info[0]
    
    #-- Create empty mask, draw filled polygon on it corresponding to largest contour ----
    # Mask is black, polygon is white
    mask = np.zeros(edges.shape)
    cv2.fillConvexPoly(mask, max_contour[0], (255))
    
    #-- Smooth mask, then blur it --------------------------------------------------------
    mask = cv2.dilate(mask, None, iterations=MASK_DILATE_ITER)
    mask = cv2.erode(mask, None, iterations=MASK_ERODE_ITER)
    mask = cv2.GaussianBlur(mask, (BLUR, BLUR), 0)
    mask_stack = np.dstack([mask]*3)    # Create 3-channel alpha mask
    
    #-- Blend masked img into MASK_COLOR background --------------------------------------
    mask_stack = mask_stack.astype('float32') / 255.0          # Use float matrices, 
    img_color = img_color.astype('float32') / 255.0                 #  for easy blending
    
    # split image into channels
    try:
        c_red, c_green, c_blue = cv2.split(img_color)
    except ValueError:
        print("seems to be greyscale already...")
        img_color = cv2.cvtColor(img_color, cv2.COLOR_GRAY2BGR)
        c_red, c_green, c_blue = cv2.split(img_color)
    
    # merge with mask got on one of a previous steps
    img_a = cv2.merge((c_red, c_green, c_blue, mask.astype('float32') / 255.0))
    
    # find bounding minimum bounding rect as that's what we want to rotate & save
    rect = cv2.minAreaRect(max_contour[0])
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    
    width = int(rect[1][0])
    height = int(rect[1][1])

    # set up a new destination exactly the size of our ROI
    src_pts = box.astype("float32")
    dst_pts = np.array([[0, height-1],
                        [0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")

    # now rotate using warp which is more efficient and preserves pixels
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warped = cv2.warpPerspective(img_a, M, (width, height))
    
    # save to disk
    basename, ext = os.path.splitext(os.path.basename(src))
    out_path = os.path.join("img-trained", basename + ".png")
    print("saving cropped cutout", out_path)
    cv2.imwrite(out_path, warped*255)

    # add to database
    if height > width:
        ratio = float(height) / width
    else:
        ratio = float(width) / height

    dbc.execute("INSERT INTO images VALUES (?, ?, ?, ?)", (ratio, width, height, out_path))


db.commit()
db.close()

