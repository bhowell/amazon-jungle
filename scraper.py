import glob
import pickle
import random
import re
import time

import cssselect
import lxml.html
import requests
import urllib.request

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0'}
selector = cssselect.HTMLTranslator().css_to_xpath("img.s-image")

with open("text-src/artifacts.pickle", "rb") as art_pick:
    artifacts = pickle.load(art_pick)

# this is a shortcut to the hi-res image versions without having to scrape the
# individual product pages
hires = re.compile("_AC_\w{5,6}_")

done = 0

for stem, wordcount in artifacts.items():
    print(done, "words completed of", len(artifacts.items()))
    done += 1
    if glob.glob("img-src/"+ stem + "_*"):
        print("we already have images for '", stem, "' skipping...")
        continue

    print("fetching", wordcount, "images for word:", stem)
    req = urllib.request.Request('https://www.amazon.com/s?k=' + stem, None, headers)
    with urllib.request.urlopen(req) as response:
        html = response.read()
        document = lxml.html.fromstring(html)
        element_list = document.xpath(selector)
        avail_count = len(element_list)
        print(avail_count, "images available for this query")
        fetch_count = wordcount + 5  # grab a few extra images for variety and fit
        if fetch_count > avail_count:
            fetch_count = avail_count
        print("fetching", fetch_count, "images...")
        for idx in range(fetch_count):
            element = element_list[idx]
            img_url = element.get("src")
            img_url = hires.sub("_AC_SL1400_", img_url)
            print("saving", img_url)
            img_ext = img_url.rsplit(".",1)[1]
            save_path = "img-src/" + stem + "_" + str(idx) + "." + img_ext
            print("to", save_path)
            save_resp = requests.get(img_url, headers=headers, allow_redirects=True)
            open(save_path, 'wb').write(save_resp.content)

    print("sleeping for a bit...")
    time.sleep(random.randint(20, 45))


