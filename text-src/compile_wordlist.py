import pickle
import pprint
import sqlite3

from nltk.corpus import wordnet as wn
import nltk

with open("jungl10.txt") as srctxt:
    booktext = srctxt.read()

artifact = wn.synset('artifact.n.01')
wordlist = []
print("tokenizing sentences")
sents = nltk.sent_tokenize(booktext)
for sent in sents:
    wordlist += nltk.word_tokenize(sent)

artifacts = {}

total_words = 0
unique_words = 0
for word in wordlist:
    if word.lower() == "chapter":
        continue  # skip chapter splits
    
    # morphy is a bit primitive, nltk.stem.PorterStemmer is prob better
    stem = wn.morphy(word)
    if not stem:
        continue

    search = True
    for syn in wn.synsets(stem):
        if (syn.pos() == "n") and search:
            if artifact in syn.hypernym_paths()[0]:
                #print(stem, "is an artifact!")
                search = False
                total_words += 1
                if stem in artifacts:
                    artifacts[stem] += 1
                else:
                    artifacts[stem] = 1
                    unique_words += 1

pprint.pp(artifacts)
print("total words", total_words)
print("unique words", unique_words)
with open("artifacts.pickle", "wb") as db:
    pickle.dump(artifacts, db)
