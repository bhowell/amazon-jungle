import glob
import pprint
import random
import sqlite3

import cv2
import numpy as np

import nltk
from nltk.corpus import wordnet
nltk.download('omw-1.4')

from PIL import Image


TESTING = False 

db = sqlite3.connect("ratios.db")
dbc = db.cursor()

def get_contours(seg_source):
    """
    given a source image return a list of contours that can be used to construct 
    """

    img = cv2.imread(seg_source)

    cut_list = []
    
    # create engine
    engine = cv2.hfs.HfsSegment_create(img.shape[0], img.shape[1])
    engine.setSlicSpixelSize(50)
    # perform segmentation
    # now "res" is a matrix of indices
    # change the second parameter to "True" to get a rgb image for "res"
    res = engine.performSegmentCpu(img, False)
    num_blobs = np.max(res)
    print("total blobs:", num_blobs)
    
    contours = []
    for idx in range(1, num_blobs+1):
        mask = cv2.compare(res, idx, cv2.CMP_EQ)
        mask = cv2.dilate(mask, None)
        mask = cv2.erode(mask, None)
        t_contours, hierarchy = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        contours += t_contours
    
    #contours, heirarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cont_sorted = sorted(contours, key=cv2.contourArea, reverse=True)
    
    cont_count = 0
    for cont in cont_sorted[1:]:  
        epsilon = 0.005*cv2.arcLength(cont, True)
        c_smooth = cv2.approxPolyDP(cont, epsilon, True)
        area = cv2.contourArea(c_smooth)
        center, dim, theta = cv2.minAreaRect(cont)

        w = dim[0]
        h = dim[1]
        
        if area < 100:
            continue
            
        # print("contour W x H:",w,h,"center", center, "theta", theta)
    
        if w > h:
            ratio = float(w)/h
            theta = theta - 90
        else:
            ratio = float(h)/w
    
        if (dim[0] > 100) or (dim[1] > 100):
            cont_count +=1
            cut = { "center": center,
                    "dim": dim,
                    "angle": theta,
                    "ratio": ratio }
            cut_list.append(cut)
    
    return cut_list


def find_artifacts(text):
    artifacts = []
    wordlist = []
    artifact = wordnet.synset('artifact.n.01')

    sents = nltk.sent_tokenize(text)
    for sent in sents:
        wordlist += nltk.word_tokenize(sent)

    for word in wordlist:
        stem = wordnet.morphy(word)
        if not stem:
            continue

        search = True
        for syn in wordnet.synsets(stem):
            if (syn.pos() == "n") and (artifact in syn.hypernym_paths()[0]) and search:
                search = False
                artifacts.append(stem)

    return artifacts


def match_artifacts(artifacts, contours):
    """
    matches up a closest fitting images to contours 
    returns:
      artifacts list with unused artifact words
      contours object with each an "artifact" field containing the filename
    """
    

    idx = 0
    for contour in contours:
        contour["idx"] = idx
        idx += 1
    # first sort them by extreme difference to median
    sorted_contours = sorted(contours, key=lambda cont: cont["ratio"])
    median = sorted_contours[round(len(contours)/2)]
    sorted_contours = sorted(contours, key=lambda cont: abs(median["ratio"] - 
                                                            cont["ratio"]),
                             reverse=True)

    artifact_map = []
    for art_idx in range(len(artifacts)):
        art = artifacts[art_idx]
        for imgfile in glob.glob("img-trained/" + art + "*.png"):
            dbc.execute("select ratio from images where img_path=?", (imgfile, ))
            ratio = dbc.fetchone()[0]
            artifact_map.append({"art": art, "art_index": art_idx,
                                 "img_file": imgfile, "ratio": ratio})

    print("len artifacts",len(artifacts))
    print("len contours:", len(sorted_contours))
    for contour in sorted_contours:
        match_dist = 200
        for art in artifact_map:
            dist = abs(art["ratio"] - contour["ratio"])
            if dist < match_dist:
                match_idx = art["art_index"]
                match_dist = dist
                # now update the canonical contour (instead of the sorted ones)
                canonical = contours[contour["idx"]]
                canonical["artifact"] = art["img_file"]
                canonical["art_ratio"] = art["ratio"]

        # now prune the artifact_map and artifacts
        artifact_map = [mapping for mapping in artifact_map if mapping["art_index"] != match_idx]
        artifacts[match_idx] = ""

    # clean out our used artifacts
    artifacts = [art for art in artifacts if art != ""]

    return artifacts, contours

src_paintings = glob.glob("paintings_src/*.jpg")

with open("text-src/jungl10.txt") as intext:
    booktext = intext.read()

illustration_count = 0

chapters = booktext.split("Chapter")[1:]
for chapter in chapters:
    print("\chapter{}")
    paragraphs = chapter[5:].split("\n\n")
    print(len(paragraphs), "paragrphs")
    text_out = ""
    artifacts = []
    chapter_done = False

    while paragraphs:
        # TODO if this is our first para in the chapter make a full page illustration
        contour_model = []
        if TESTING:
            contour_model = get_contours("test-painting.png")
            with Image.open("test-painting.png") as im:
                collage_size = im.size
            #for count in range(random.randint(20, 30)):
            #    cont = {}
            #    cont["ratio"] = 4 * (1 + random.random())
            #    contour_model.append(cont)
        else:
            illustration_count += 1
            painting = random.choice(src_paintings)
            print("illustration src:", painting)
            contour_model = get_contours(painting)
            with Image.open(painting) as im:
                collage_size = im.size


        # make a list of artifacts long enough to complete one collage
        while len(artifacts) < len(contour_model):
            try:
                next_para = paragraphs.pop(0)
            except IndexError as e:
                chapter_done = True
                break
            text_out += next_para + "\n\n"
            artifacts += find_artifacts(next_para)

        if chapter_done:
            break

        # match up artifacts and contours
        artifacts, contour_model = match_artifacts(artifacts, contour_model)
        print(text_out)

        collage = Image.new("RGBA", collage_size, "white")
        for cont in contour_model:
            cutting = Image.open(cont["artifact"])

            target_w = round(cont["dim"][0])
            target_h = round(target_w * cont["art_ratio"])
            cutting = cutting.resize((target_w, target_h))

            cutting = cutting.rotate(cont["angle"], expand=True)

            target_x = round(cont["center"][0] - (cutting.size[0] / 2))
            target_y = round(cont["center"][1] - (cutting.size[1] / 2))

            collage.paste(cutting, (target_x, target_y), cutting)

        if TESTING:
            pprint.pprint(contour_model)
            collage.save("/tmp/test.png")
            break
        else:
            illus_file = "/tmp/jungle" + str(illustration_count) + ".png"
            print("<ILLUSTRATION>", illus_file)
            collage.save(illus_file)

    print("\n\n___")
    if TESTING:
        break
